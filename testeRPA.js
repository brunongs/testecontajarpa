const puppeteer = require('puppeteer');
const fs = require('fs');
var path = require('path');

(async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto('https://unsplash.com/');

    await imageDownload(page);

    await page.waitForTimeout(10000);

    await DownloadMgr(page, path.resolve('../../../Downloads'));

    await browser.close();

    await createFolder("../Teste Contajá");

    await copyFile('../../../Downloads/nubelson-fernandes-Yh2Y8avvPec-unsplash.jpg', '../Teste Contajá/Negocio.png');
})();

async function imageDownload(page) {
    await page.type('#app > div > header > nav > div.hzGh7 > form > div.ejG8W.kypwe > input', 'Business');
    await page.click('#app > div > header > nav > div.hzGh7 > form > button:nth-child(1)');
    await page.waitForSelector('#app > div > div:nth-child(3) > div:nth-child(3) > div:nth-child(1) > div > div > div > div:nth-child(1) > div > figure > div > div.L34o8 > div > div > a > div > div.MorZF > div > img');
    await page.click('#app > div > div:nth-child(3) > div:nth-child(3) > div:nth-child(1) > div > div > div > div:nth-child(1) > div > figure > div > div.L34o8 > div > div > a > div > div.MorZF > div > img');
    await page.waitForSelector('body > div.ReactModalPortal > div > div > div.Lvlem.fBS9b > div > div > div:nth-child(1) > div.IDj6P.voTTC > header > div.EdCFo > div.IDBf5.ckD_t.i8_xY > div > a > span');
    await page.click('body > div.ReactModalPortal > div > div > div.Lvlem.fBS9b > div > div > div:nth-child(1) > div.IDj6P.voTTC > header > div.EdCFo > div.IDBf5.ckD_t.i8_xY > div > a > span');
};

async function DownloadMgr(page, downloaddPath) {
    if(!fs.existsSync(downloaddPath)){
        fs.mkdirSync(downloaddPath);
    }
    this.download = async function(url) {
        await init;
        try{
            await page.goto(url);
        }catch(e){}
        return Promise.resolve();
    }
};

async function createFolder(path) {
    fs.existsSync(path) || fs.mkdirSync(path);
};

async function copyFile(from, to) {
    fs.copyFile(from, to, (err) => {
        if (err) throw err;
        console.log('Arquivo renomeado e copiado para área de trabalho.');
        deleteFile(from);
    });
};

async function deleteFile(file) {
    fs.unlink(file, function (err) {
        if (err) throw err;
        console.log('Arquivo deletado da pasta downloads!');
    });
};